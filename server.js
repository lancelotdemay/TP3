// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var jwt = require('jsonwebtoken');
var sqlite3 = require('sqlite3').verbose();
var app = express();
app.use(cookieParser());
var db = new sqlite3.Database('db.sqlite');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
	if(req.cookies.token)
    {
        var decoded_user = jwt.verify(req.cookies.token, 'shhhhhh');

        db.all('SELECT * FROM users WHERE ident = ? AND password = ?', [decoded_user.ident, decoded_user.password], function(err, data) {
            if(data.length >= 1)
            {
                res.send('Bonjour '+ decoded_user.ident)
            }
        });

    }
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next) {
	var users = db.all('SELECT rowid FROM users WHERE ident = ? AND password = ?', [req.body.ident, req.body.password], function(err, data) {
        if(data.length >= 1)
        {
			var token = jwt.sign({ident : req.body.ident, password : req.body.password}, 'shhhhhh');
			db.all('INSERT INTO sessions VALUES (?)', token, function(err,data) {
				res.cookie('token', token).send('cookie set');
			});
		}
		else
		{
			res.send(JSON.stringify({status : false}));
		}
	});

});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
